This is a fork from Thomas Mailund's ziphmm library: https://github.com/mailund/ziphmm

Only the C++ library is kept and compilation was ported to CMake. A few more functions were added.
